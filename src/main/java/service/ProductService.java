/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dao.ProductDAO;
import java.util.ArrayList;
import model.Product;
import utils.IConstants;


/**
 *
 * @author be_me
 */
public class ProductService {
    
    public ArrayList<Product> getHomePageProducts(){
        
        ProductDAO pDao = new ProductDAO();
        return pDao.getTopProducts(6);
        
    }
    
    public ArrayList<Product> getMenProducts(){
        
        ProductDAO pDao = new ProductDAO();
        return pDao.getMenProducts(6);
        
    }
    public ArrayList<Product> getWomenProducts(){
        
        ProductDAO pDao = new ProductDAO();
        return pDao.getWomenProducts(6);
        
    }
    
 
    
    public ArrayList<Product> getAllProducts(){
       
       ProductDAO pDao = new ProductDAO();
        ArrayList<Product> productList = pDao.getAllProducts();
        return productList;
        
        
    }
    
    
    public Product getProduct(long productId){
        ProductDAO productDAO = new ProductDAO();
        Product product = productDAO.getProductById(productId);
        return product;
    }
    
    public void insertProduct(Product nProduct){
        ProductDAO pDao = new ProductDAO();
        pDao.insertProduct(nProduct);
        return;
        
    }
    
    public void updateProduct(Product nProduct){
        ProductDAO pDao = new ProductDAO();
        pDao.updateProduct(nProduct);
        return;
        
    }
    
    public void deleteProduct(long productId){
        ProductDAO pDao = new ProductDAO();
        pDao.deleteProduct(productId);
        return;
        
    }

    }
        
    

