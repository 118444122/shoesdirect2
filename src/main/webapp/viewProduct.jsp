<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.User"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Shoes Direct</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/shop-item.css" rel="stylesheet">
  <style>
      body{
           background: url("https://static.vecteezy.com/system/resources/previews/000/582/344/non_2x/vector-luxury-polygonal-pattern-and-gold-triangles-lines-with-lighting-on-dark-background-geometric-low-polygon-gradient-shapes.jpg") !important;

      }
  </style>

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="#">Shoes Direct</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <c:if test="${empty SKUSER.firstName}">
                                <a class="nav-link" href="./login.jsp">Login
                                    <span class="sr-only">(current)</span>
                                </a>
                            </c:if>
                            <c:if test="${not empty SKUSER.firstName}">
                                <a class="nav-link" href="./login.jsp">Hi ${SKUSER.firstName}
                                    <span class="sr-only">(current)</span>
                                </a>
                            </c:if>
                        </li>
                        
                                   <li class="nav-item dropdown">
            <img alt="shoe" src=images/cart.png class="nav-link dropdown-toggle"  id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPages">
                  <h5>Cart</h5>
                  <form action="${addToCartUrl}" method="POST">
                  <p class="dropdown-item" id="demo"></p>
                  <p id="cart1" class="dropdown-item">${oldProduct.name} x ${oldProduct.price} </p>
              
              
              <a  href="checkout.jsp">Checkout</a><p class="dropdown-item" id="priceTotal"></p>
              <p class="dropdown-item">Cart Total ${oldProduct.price} </p>
              
              <script>
    function addtocart() {
  var x = document.getElementById("cart1");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
    }</script>
              
              
              
              </div>
          </li>
                    </ul>
                </div>
            </div>
        </nav>


  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <div class="col-lg-3">
          <h1 class="my-4" style="color:white;">Shoes Direct</h1>
        
      </div>
      <!-- /.col-lg-3 -->

      <div class="col-lg-9">
          <form action="${insertProductUrl}" method="POST">
        <div class="card mt-4">
          <img class="card-img-top img-fluid"  src="images/${oldProduct.imageLocation}" alt="">
          <div class="card-body">
            <h3 class="card-title">${oldProduct.name}</h3>
            <h4>${oldProduct.price}</h4>
            <p class="card-text">${oldProduct.description}</p>
            <p class="card-text">${oldProduct.category}</p>
            <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
            4.0 stars
          </div>
        </div>
        <!-- /.card -->

        <div class="card card-outline-secondary my-4">
          <div class="card-header">
            Get them before they are gone!
          </div>
          <div class="card-body">
         
              <a href="" onclick="cart1()" class="btn btn-success">Add to Cart</a>
          </div>
        </div>
        <!-- /.card -->

      </div>
      <!-- /.col-lg-9 -->

    </div>

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
