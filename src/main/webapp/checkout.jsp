<%-- 
    Document   : checkout
    Created on : 9 Dec 2020, 17:19:42
    Author     : dunlea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Checkout</title>
    <link rel="stylesheet" href="css/checkout.css">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script language = "JavaScript"></script>
  </head>
  <body>
    <div class="container">
      <header>Checkout</header>
      <div class="progress-bar">
        <div class="step">
          <p>
Contact</p>
<div class="bullet">
            <span>1</span>
          </div>
<div class="check fas fa-check">
</div>
</div>
<div class="step">
          <p>
Delivery</p>
<div class="bullet">
            <span>2</span>
          </div>
<div class="check fas fa-check">
</div>
</div>
<div class="step">
          <p>
Payment</p>
<div class="bullet">
            <span>3</span>
          </div>
<div class="check fas fa-check">
</div>
</div>
<div class="step">
          <p>
Order</p>
<div class="bullet">
            <span>4</span>
          </div>
<div class="check fas fa-check">
</div>
</div>
</div>
<div class="form-outer">
    <form name="myCheckout" action="index.html"  method="post"  >
          <div class="page slide-page">
            <div class="title">
Contact:</div>
<div class="field">
              <div class="label">
Full Name</div>
<input id="fullName"  type="text">
            </div>
<div class="field">
              <div class="label">
Email</div>
<input type="text">
            </div>
      <div class="field">
              <div class="label">
Phone Number</div>
          <input type="tel" pattern="[0-9]{10}" id="phoneNo" required="required">
            </div>
<div class="field">
              <button  onsubmit="return validateForm2()"  type="submit" class="firstNext next">Next</button>
            </div>
        
    
</div>
<div class="page">
            <div class="title">
Delivery Info:</div>
<div class="field">
              <div class="label">
Address</div>
<input type="text">
            </div>
    <div class="field">
              <div class="label">
County</div>
<input type="text">
            </div>
    <div class="field">
              <div class="label">
Eircode</div>
        <input type="text"><div style="float: right; position: absolute; right: -300px !important; " >Regular-5-8  days - free<input label="fast" placeholder="fast" type="radio"></div><div style="position: absolute; float: right; top:-90px;  right: -300px !important;" >Prime DPD-1-2  days - €5<input label="faster" placeholder="faster" type="radio"></div>

            </div>
    

    
<div class="field btns">
              <button class="prev-1 prev">Previous</button>
              <button class="next-1 next">Next</button>
            </div>
</div>
<div class="page">
            <div class="title">
Payment:</div>
<div class="field">
              <div class="label">
Card number</div>
<input type="text">
            </div>
<div class="field">
              <div class="label">
Expire date</div>
    <input  maxlength="10" size="10" placeholder="Month" type="text"><input maxlength="4" size="4" placeholder="Year" type="text">
            </div>
    <div class="field">
              <div class="label">
Cvv</div>
        <input maxlength="3" size="3" type="text">
            </div>
    
        
          
    
<div class="field btns">
              <button class="prev-2 prev">Previous</button>
              <button class="next-2 next">Pay</button>
            </div>
</div>
<div class="page">
            <div class="orderComplete">
Your order is complete!</div>

    <div>
        <p class="orderComplete">Thank you for shopping with shoes Direct!</p><p>We have sent 
            you your order details to your email along with your tracking number.</p></div>
            
<div class="submit">
              
              <button type="submit" class="submit">Return to Shoes Direct</button>
            </div>
</div>
</form>
</div>
</div>

<script src="checkout.js"></script> 

  </body>
</html>
