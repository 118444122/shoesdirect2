<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.User"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <%-- 

added css and script


--%>
     <link rel="stylesheet" href="css/style.css">
  <script src="https://www.w3schools.com/lib/w3.js"></script>
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/shopmain.css" rel="stylesheet">
   <link href="css/modern-business.css" rel="stylesheet">
 
  
   <style>
.btnCart {
  border: none;
  outline: 0;
  padding: 12px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}

.btnCart:hover {
  opacity: 0.7;
}
</style>
<script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="js/vendor/scripts.js"></script>
  <script src="script.js"></script>
  <%-- 

added css and script


--%>
  
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Shoes Direct</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/shop-homepage.css" rel="stylesheet">
        <style>
            .row{
                background-color: black;
                background: url("https://static.vecteezy.com/system/resources/previews/000/582/344/non_2x/vector-luxury-polygonal-pattern-and-gold-triangles-lines-with-lighting-on-dark-background-geometric-low-polygon-gradient-shapes.jpg") !important;
            }
            .goog-te-banner-frame.skiptranslate {
                display: none !important;
            }
            
        </style>

    </head>

    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="#">Shoes Direct</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <c:if test="${empty SKUSER.firstName}">
                                <a class="nav-link" href="./login.jsp">Login
                                    <span class="sr-only">(current)</span>
                                </a>
                            </c:if>
                            <c:if test="${not empty SKUSER.firstName}">
                                <a class="nav-link" href="./login.jsp">Hi ${SKUSER.firstName}
                                    <span class="sr-only">(current)</span>
                                </a>
                            </c:if>
                        </li>
<script type="text/javascript" src="jquery-1.8.2.min.js"></script>


 <li><a href="#googtrans(en|en)" class="lang-en lang-select" data-lang="en"><img src="images/english1.jpeg" alt="English"></a></li>
 <li><a href="#googtrans(en|fr)" class="lang-es lang-select" data-lang="fr"><img src="images/french3.png" alt="French"></a></li>

<script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({ layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }

    function triggerHtmlEvent(element, eventName) {
      var event;
      if (document.createEvent) {
        event = document.createEvent('HTMLEvents');
        event.initEvent(eventName, true, true);
        element.dispatchEvent(event);
      } else {
        event = document.createEventObject();
        event.eventType = eventName;
        element.fireEvent('on' + event.eventType, event);
      }
    }

    jQuery('.lang-select').click(function() {
      var theLang = jQuery(this).attr('data-lang');
      jQuery('.goog-te-combo').val(theLang);

      //alert(jQuery(this).attr('href'));
      window.location = jQuery(this).attr('href');
      location.reload();

    });
</script>
<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        <li class="nav-item dropdown">
            <img alt="shoe" src=images/cart.png class="nav-link dropdown-toggle"  id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPages">
                  <h5>Cart</h5>
                  <p class="dropdown-item" id="demo"></p>
              <p class="dropdown-item" id="demo2"></p>
              <p class="dropdown-item" id="demo3"></p>
              <p class="dropdown-item" id="demo4"></p>
              <p class="dropdown-item" id="demo5"></p>
              <p class="dropdown-item" id="demo6"></p>
              
              <a  href="checkout.jsp">Checkout</a><p class="dropdown-item" id="priceTotal"></p>
              
              
              
              </div>
          </li>
          <li class="nav-item dropdown">
          <div class="cart-nav2"><div class="icon2"></div>
            <input type="text" id="number" class="item-count" value="0"/>
          </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Page Content -->
        <div class="container bg-light">

            <div class="row">

                <div class="col-lg-3">

                    <h1 class="my-4">Hurry sale ends in!!</h1>
                    
                    <div  class="wrapper1">
      <div class="display1">
        <div  id="time">
</div>
</div>

<span></span>
      <span></span>
    </div>
                    <br>
                    <div>
                        <h1>Categories</h1>
                    </div>
                    <div class="list-group">
                        <a href="men" class="list-group-item">Men</a>
                        <a href="women" class="list-group-item">Women</a>
                        
                    </div>


                </div>
                <!-- /.col-lg-3 -->

                <div class="col-lg-9">
                    <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
            <h2>See Whats In Store!</h2>
          <ol class="carousel-indicators">
             
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <img class="d-block img-fluid" src="https://i8.amplience.net/i/jpl/jd_356016_a?qlt=92&w=750&h=531&v=1"  alt="First slide" width="1200" height="850">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="https://i8.amplience.net/i/jpl/jd_294023_a?qlt=92&w=750&h=531&v=1"  alt="Second slide" width="1200" height="850">
            </div>
            <div class="carousel-item">
              <img class="d-block img-fluid" src="https://i8.amplience.net/i/jpl/jd_387843_a?qlt=92&w=750&h=531&v=1%201x,%20https://i8.amplience.net/i/jpl/jd_387843_a?qlt=92&w=950&h=673&v=1%202x,%20https://i8.amplience.net/i/jpl/jd_387843_a?qlt=92&w=1200&h=850&v=1%203x"  alt="Third slide" width="1200" height="850">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

                    <div class="row">

                        <c:forEach items="${products}" var="topProduct">
                            <div class="col-lg-4 col-md-6 mb-4">
                                <div class="card h-100">
                                    <a href="#"><img class="card-img-top" src="images/${topProduct.imageLocation}" alt=""></a>
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <a href="#">${topProduct.name}</a>
                                        </h4>
                                        <h5>€${topProduct.price}</h5>
                                        <p class="card-text">${topProduct.description}</p>
                                        <p class="card-text">${topProduct.category}</p>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                                    </div>
                                     <c:url value="/productAdmin" var="viewProductUrl">
                                        <c:param name="action" value="view"/>
                                        <c:param name="id" value="${topProduct.id}"/>
                                    </c:url>
                                    <a href="${viewProductUrl}"> View</a> 
                                </div>
                            </div>
                        </c:forEach>


                    </div>
                    <!-- /.row -->

                </div>
                <!-- /.col-lg-9 -->

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

        <!-- Footer -->
        <footer>      <div class="main-content">
        <div class="left box">
          <h2>
About us</h2>
<div class="content">
            <p>
Shoes Direct is an online shoe shop with the latest new sneakers and the biggest bargains. We were founded in 2020 during the pandemic as it gave us inspiration to create the worlds biggest online shoe shop!</p>
<div class="social">
              <a href=""><span class="fab fa-facebook-f"></span></a>
              <a href="#"><span class="fab fa-twitter"></span></a>
              <a href=""><span class="fab fa-instagram"></span></a>
              <a href=""><span class="fab fa-youtube"></span></a>
            </div>
</div>
</div>
<div class="center box">
          <h2>
Address</h2>
<div class="content">
            <div class="place">
              <span class="fas fa-map-marker-alt"></span>
              <span class="text">Gaol Walk, University College, Cork,</span>
            </div>
<div class="phone">
              <span class="fas fa-phone-alt"></span>
              <span class="text">+021-1830046</span>
            </div>
<div class="email">
              <span class="fas fa-envelope"></span>
              <span class="text">ShoesDirect@Gmail.com</span>
            </div>
</div>
</div>
<div class="right box">
          <h2>
Contact us</h2>
<div class="content">
            <form action="#">
              <div class="email">
                <div class="text">
Email *</div>
<input type="email" required>
              </div>
<div class="msg">
                <div class="text">
Message *</div>
<!-- NOTE: Due to more textarea tag I got an error. So I changed the textarea name to changeit. Please change that changeit name to textarea -->
<textarea id=".msgForm" rows="2" cols="25" required></textarea> <!-- replace this changeit name to textarea -->
              
              <br />
<div class="btn">
<button type="submit">Send</button>
              </div>
<div class="bottom">
<center>
          <span class="credit">Created By <a href="https://youtube.com/c/codingnepal">ShoesDirect</a> | </span>
          <span class="far fa-copyright"></span> 2020 All rights reserved.
        </center>
</div></footer>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    </body>
    <script> 
    const time = document.querySelector(".display1 #time");
var deadline = new Date("Dec 5, 2021 24:00:00").getTime(); 
var x = setInterval(function() { 
var now = new Date().getTime(); 
var t = deadline - now; 
var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
var seconds = Math.floor((t % (1000 * 60)) / 1000); 
document.getElementById("time").innerHTML = hours + "h " + minutes + "m " + seconds + "s "; 
    if (t < 0) { 
        clearInterval(x); 
        document.getElementById("time").innerHTML = "EXPIRED"; 
    } 
}, 1000); 
</script> 

</html>
